## Virgin Money coding test

Search for photos using Flickr api.

---

## Usage

Instructions to use the app

1. Launch the app and enter search keyword.
2. Select Search button to view photos.
3. Photos are displayed in a 3 column grid with image and title at the bottom for each photo.
4. Select action bar up button or system back button to go back to previous screen.

---

## Implementation details

Read here for implementation details

1. SearchActivity is the Main activity launched when app icon is selected.
2. It uses Material design components like TextInputLayout, Button and Alert dialog.
3. Very basic error handling is added. Alert dialog is displayed when Search button is selected with empty text and more than a specific number of characters are entered in text field.
4. PhotoActivity displays progress bar when search results are fetched using Flickr api.
5. Photos are displayed as a 3 column grid using Recyclerview.
6. Glide library is used to show default placeholder and for lazy loading of images.
7. Jetpack Paging2 library is used to implement pagination.
8. Retrofit library with RxJava is used for network calls.
9. Unit tests, Instrumented tests and UI tests cover testing to some extent.

---

## Necessary improvements to be done with more time

Following things are not implemented for this test due to lack of time and require more effort.

1. Pull to refresh pattern in PhotosActivity. This allows user to fetch updated list of photos using the same search text.
2. Landscape mode should be supported with a different layout if necessary.
3. Dependency library Hilt or Dagger should be used to construct and inject dependencies.
4. Input validation and error handling should be added.
5. More unit tests, integration tests and UI tests should be added.
6. Logging with HttpLoggingInterceptor should only be for debug builds and should not be a part of release builds.
