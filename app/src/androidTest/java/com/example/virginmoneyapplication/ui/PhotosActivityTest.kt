package com.example.virginmoneyapplication.ui

import android.app.Application
import android.content.Intent
import android.view.View
import android.widget.ProgressBar
import androidx.arch.core.executor.testing.CountingTaskExecutorRule
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.platform.app.InstrumentationRegistry
import com.example.virginmoneyapplication.DefaultServiceLocator
import com.example.virginmoneyapplication.R
import com.example.virginmoneyapplication.ServiceLocator
import com.example.virginmoneyapplication.api.FlickrApi
import com.example.virginmoneyapplication.repository.FakeFlickrApi
import com.example.virginmoneyapplication.repository.FlickrApiResponseFactory
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

/**
 * Test in this class starts PhotosActivity using Intent with search text as an Intent extra and
 * waits till recyclerview adapter has search results then verifies that recyclerview is displayed.
 */
class PhotosActivityTest {

    @get:Rule
    var testRule = CountingTaskExecutorRule()

    private val apiResponseFactory = FlickrApiResponseFactory()

    @Before
    fun setup() {
        val fakeApi = FakeFlickrApi()
        apiResponseFactory.run {
            addPhoto(createPhoto())
            addPhoto(createPhoto())
            addPhoto(createPhoto())
        }
        val searchResults = apiResponseFactory.createFlickrApiResponse()
        fakeApi.addResult(searchResults)
        val app = ApplicationProvider.getApplicationContext<Application>()
        ServiceLocator.swap(object : DefaultServiceLocator(app)  {
            override fun getFlickrApi(): FlickrApi {
                return fakeApi
            }
        })
    }

    @Test
    @Throws(InterruptedException::class, TimeoutException::class)
    fun showResults() {
        val intent = Intent(ApplicationProvider.getApplicationContext(), PhotosActivity::class.java).apply {
            putExtra(PhotosActivity.EXTRA_SEARCH_TEXT, "Cat")
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
        val activity = InstrumentationRegistry.getInstrumentation().startActivitySync(intent)
        val recyclerView = activity.findViewById<RecyclerView>(R.id.recyclerView)
        val progressBar = activity.findViewById<ProgressBar>(R.id.loadingProgressBar)
        assertThat(recyclerView.adapter, notNullValue())
        waitForAdapterChange(recyclerView)
        assertThat(recyclerView.visibility, `is`(View.VISIBLE))
        assertThat(progressBar.visibility, `is`(View.GONE))
    }

    private fun waitForAdapterChange(recyclerView: RecyclerView) {
        val latch = CountDownLatch(1)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            recyclerView.adapter?.registerAdapterDataObserver(
                object : RecyclerView.AdapterDataObserver() {
                    override fun onChanged() {
                        latch.countDown()
                    }

                    override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                        latch.countDown()
                    }
                })
        }
        testRule.drainTasks(1, TimeUnit.SECONDS)
        if (recyclerView.adapter?.itemCount ?: 0 > 0) {
            return
        }
        assertThat(latch.await(10, TimeUnit.SECONDS), `is`(true))
    }
}