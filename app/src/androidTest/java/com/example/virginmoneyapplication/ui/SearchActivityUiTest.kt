package com.example.virginmoneyapplication.ui

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.example.virginmoneyapplication.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * UI automation test. Executing this test installs app to device, launches it, types search word in
 * the text field and clicks Search button in SearchActivity. It verifies that progress bar is
 * displayed in the PhotosActivity.
 */
@RunWith(AndroidJUnit4::class)
@LargeTest
class SearchActivityUiTest {

    @get:Rule var activityScenarioRule = activityScenarioRule<SearchActivity>()

    @Test
    fun enterSearchTextAndClickButton_newActivity() {
        onView(withId(R.id.search_edit_text)).perform(typeText("Cat"), closeSoftKeyboard())
        onView(withId(R.id.search_button)).perform(click())

        onView(withId(R.id.loadingProgressBar)).check(matches(isDisplayed()))
    }
}