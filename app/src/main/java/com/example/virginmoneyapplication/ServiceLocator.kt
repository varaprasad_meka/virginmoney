package com.example.virginmoneyapplication

import android.app.Application
import android.content.Context
import androidx.annotation.VisibleForTesting
import com.example.virginmoneyapplication.api.FlickrApi
import com.example.virginmoneyapplication.repository.FlickrSearchPhotosRepository

/**
 * Simplified service locator implementation to allow replacing default implementations
 * for testing.
 */
interface ServiceLocator {
    companion object {
        private val LOCK = Any()
        private var instance: ServiceLocator? = null
        fun instance(context: Context): ServiceLocator {
            synchronized(LOCK) {
                if (instance == null) {
                    instance = DefaultServiceLocator(app = context.applicationContext as Application)
                }
                return instance!!
            }
        }

        /**
         * Allows tests to replace the default implementations.
         */
        @VisibleForTesting
        fun swap(locator: ServiceLocator) {
            instance = locator
        }
    }

    fun getRepository(): FlickrSearchPhotosRepository

    fun getFlickrApi(): FlickrApi
}

open class DefaultServiceLocator(val app: Application) : ServiceLocator {

    private val api by lazy {
        FlickrApi.create()
    }

    override fun getRepository(): FlickrSearchPhotosRepository {
        return FlickrSearchPhotosRepository(flickrApi = getFlickrApi(), )
    }

    override fun getFlickrApi(): FlickrApi {
        return api
    }
}