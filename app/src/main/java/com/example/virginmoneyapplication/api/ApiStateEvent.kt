package com.example.virginmoneyapplication.api

sealed class ApiStateEvent {

    class StateLoading() : ApiStateEvent()
    class StateDone() : ApiStateEvent()
    class StateError() : ApiStateEvent()
}