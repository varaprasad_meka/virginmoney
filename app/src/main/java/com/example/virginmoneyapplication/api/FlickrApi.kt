package com.example.virginmoneyapplication.api

import io.reactivex.Single
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickrApi {

    @GET("/services/rest/")
    fun fetchSearchPhotoResults(
        @Query("method") method: String = "flickr.photos.search",
        @Query("api_key") key: String = "96358825614a5d3b1a1c3fd87fca2b47",
        @Query("text") searchTag: String = "kittens",
        @Query("per_page") perPage: Int = 30,
        @Query("page") pageNo: Int = 1,
        @Query("extras") extras: String = "url_s",
        @Query("format") format: String = "json",
        @Query("nojsoncallback") noJsonCallback: String = "1"): Single<FlickrApiResponse>

    companion object {

        private const val BASE_URL = "https://api.flickr.com/"

        fun create(): FlickrApi {
            val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            val httpClient = OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .build()

            return Retrofit.Builder().apply {
                baseUrl(BASE_URL)
                client(httpClient)
                addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                addConverterFactory(GsonConverterFactory.create())
            }.build().create(FlickrApi::class.java)
        }
    }
}