package com.example.virginmoneyapplication.api

import com.google.gson.annotations.SerializedName

data class FlickrApiResponse(@SerializedName("photos") val photosDetails: PhotosDetails)

data class PhotosDetails(@SerializedName("page") val page: Int,
                         @SerializedName("pages") val totalPages: Int,
                         @SerializedName("perpage") val photosPerPage: Int,
                         @SerializedName("total") val totalPhotos: String?,
                         @SerializedName("photo") val photoList: List<Photo>?)

data class Photo(@SerializedName("id") val id: String?,
                 @SerializedName("owner") val owner: String?,
                 @SerializedName("secret") val secret: String?,
                 @SerializedName("server") val server: String?,
                 @SerializedName("farm") val farm: Int,
                 @SerializedName("title") val title: String?,
                 @SerializedName("ispublic") val isPublic: Int,
                 @SerializedName("url_s") val imageUrl: String?)