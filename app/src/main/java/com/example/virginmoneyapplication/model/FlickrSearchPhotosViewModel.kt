package com.example.virginmoneyapplication.model

import androidx.lifecycle.*
import com.example.virginmoneyapplication.repository.FlickrSearchPhotosRepository
import com.example.virginmoneyapplication.repository.Listing
import io.reactivex.disposables.CompositeDisposable

class FlickrSearchPhotosViewModel(val repository: FlickrSearchPhotosRepository,
                                  val compositeDisposable: CompositeDisposable) : ViewModel() {

    private val queryLiveData = MutableLiveData<String>()

    private val repoResult: LiveData<Listing> = queryLiveData.map {
        repository.searchPhotos(it, compositeDisposable)
    }

    val searchResults = repoResult.switchMap { it.pagedList }
    val networkState = repoResult.switchMap { it.networkState }

    fun searchPhoto(queryString: String) {
        queryLiveData.postValue(queryString)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}