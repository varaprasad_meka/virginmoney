package com.example.virginmoneyapplication.model

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.virginmoneyapplication.repository.FlickrSearchPhotosRepository
import io.reactivex.disposables.CompositeDisposable

class FlickrSearchViewModelFactory(
    private val repository: FlickrSearchPhotosRepository,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FlickrSearchPhotosViewModel(repository, compositeDisposable) as T
    }
}