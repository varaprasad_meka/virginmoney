package com.example.virginmoneyapplication.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.virginmoneyapplication.api.ApiStateEvent
import com.example.virginmoneyapplication.api.FlickrApi
import com.example.virginmoneyapplication.api.Photo
import io.reactivex.disposables.CompositeDisposable

class FlickrDataSource(private val flickrService: FlickrApi,
                       private val compositeDisposable: CompositeDisposable,
                       private val searchText: String) : PageKeyedDataSource<Int, Photo>() {

    val networkState: MutableLiveData<ApiStateEvent> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Photo>) {
        updateState(ApiStateEvent.StateLoading())
        compositeDisposable.add(flickrService.fetchSearchPhotoResults(searchTag = searchText,
            pageNo = 1, perPage = params.requestedLoadSize).subscribe({
            updateState(ApiStateEvent.StateDone())
            callback.onResult(it.photosDetails.photoList!!, null, 2)
        }, {
            updateState(ApiStateEvent.StateError())
        }))
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Photo>) {
        compositeDisposable.add(flickrService.fetchSearchPhotoResults(searchTag = searchText,
            pageNo = params.key, perPage = params.requestedLoadSize).subscribe({
            if (it.photosDetails.page > 1) {
                callback.onResult(it.photosDetails.photoList!!, params.key - 1)
            } else {
                // handle first page
                callback.onResult(it.photosDetails.photoList!!, null)
            }
        }, {
            updateState(ApiStateEvent.StateError())
        }))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Photo>) {
        compositeDisposable.add(flickrService.fetchSearchPhotoResults(searchTag = searchText,
            pageNo = params.key, perPage = params.requestedLoadSize).subscribe({
            if (it.photosDetails.page < it.photosDetails.totalPages) {
                callback.onResult(it.photosDetails.photoList!!, params.key + 1)
            } else {
                // handle last page
                callback.onResult(it.photosDetails.photoList!!, null)
            }
        }, {
            updateState(ApiStateEvent.StateError())
        }))
    }

    private fun updateState(state: ApiStateEvent) {
        networkState.postValue(state)
    }
}