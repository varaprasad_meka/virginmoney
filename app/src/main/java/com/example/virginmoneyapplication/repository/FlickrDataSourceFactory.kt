package com.example.virginmoneyapplication.repository

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.virginmoneyapplication.api.FlickrApi
import com.example.virginmoneyapplication.api.Photo
import io.reactivex.disposables.CompositeDisposable

class FlickrDataSourceFactory(private val compositeDisposable: CompositeDisposable,
                              private val flickrService: FlickrApi,
                              private val searchText: String) : DataSource.Factory<Int, Photo>() {

    val flickrDataSourceLiveData = MutableLiveData<FlickrDataSource>()

    override fun create(): DataSource<Int, Photo> {
        val dataSource = FlickrDataSource(flickrService, compositeDisposable, searchText)
        flickrDataSourceLiveData.postValue(dataSource)
        return dataSource
    }
}