package com.example.virginmoneyapplication.repository

import androidx.lifecycle.switchMap
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.virginmoneyapplication.api.FlickrApi
import io.reactivex.disposables.CompositeDisposable

class FlickrSearchPhotosRepository(private val flickrApi: FlickrApi) {

    fun searchPhotos(searchText: String, compositeDisposable: CompositeDisposable): Listing {
        val config = PagedList.Config.Builder().apply {
            setPageSize(PAGE_SIZE)
            setEnablePlaceholders(false)
        }.build()

        val dataSourceFactory = FlickrDataSourceFactory(compositeDisposable = compositeDisposable,
            flickrService = flickrApi, searchText)

        val livePagedList = LivePagedListBuilder(dataSourceFactory, config).build()
        return Listing(
            pagedList = livePagedList,
            networkState = dataSourceFactory.flickrDataSourceLiveData.switchMap {
                it.networkState
            })
    }

    companion object {
        const val PAGE_SIZE = 30
    }
}