package com.example.virginmoneyapplication.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.example.virginmoneyapplication.api.ApiStateEvent
import com.example.virginmoneyapplication.api.Photo

data class Listing(
    val pagedList: LiveData<PagedList<Photo>>,
    val networkState: LiveData<ApiStateEvent>)