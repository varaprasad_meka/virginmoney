package com.example.virginmoneyapplication.ui

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.virginmoneyapplication.R
import com.example.virginmoneyapplication.api.Photo

class FlickrPhotoListAdapter(
    private val context: Context) : PagedListAdapter<Photo, FlickrPhotoListAdapter.PhotoViewHolder>(REPO_COMPARATOR){

    class PhotoViewHolder(photoCard: View) : RecyclerView.ViewHolder(photoCard) {
        val photo: ImageView = photoCard.findViewById(R.id.photo)
        val title: TextView = photoCard.findViewById(R.id.title)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): PhotoViewHolder {
        val photoCard = LayoutInflater.from(context).inflate(R.layout.view_flickr_photo, parent, false)
        return PhotoViewHolder(photoCard)
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val photo = getItem(position)

        holder.title.text = photo?.title
        Glide.with(context)
            .load(photo?.imageUrl)
            .centerCrop()
            .placeholder(ColorDrawable(Color.LTGRAY))
            .into(holder.photo)
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean =
                oldItem == newItem
        }
    }
}