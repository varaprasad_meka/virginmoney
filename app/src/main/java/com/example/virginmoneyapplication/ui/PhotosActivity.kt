package com.example.virginmoneyapplication.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.virginmoneyapplication.api.ApiStateEvent
import com.example.virginmoneyapplication.model.FlickrSearchPhotosViewModel
import com.example.virginmoneyapplication.model.FlickrSearchViewModelFactory
import com.example.virginmoneyapplication.R
import com.example.virginmoneyapplication.ServiceLocator
import io.reactivex.disposables.CompositeDisposable

class PhotosActivity : AppCompatActivity() {

    private val viewModel: FlickrSearchPhotosViewModel by viewModels {
        FlickrSearchViewModelFactory(ServiceLocator.instance(this).getRepository(),
            CompositeDisposable())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photos)
        setTitle("Photos")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val progressBar = findViewById<ProgressBar>(R.id.loadingProgressBar)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)

        recyclerView.layoutManager = GridLayoutManager(this, 3)
        val adapter = FlickrPhotoListAdapter(this)
        recyclerView.adapter = adapter

        viewModel.searchResults.observe(this, Observer {
            adapter.submitList(it)
        })

        viewModel.networkState.observe(this, Observer {
            when(it) {
                is ApiStateEvent.StateLoading -> {
                    recyclerView.visibility = View.GONE
                    progressBar.visibility = View.VISIBLE
                }
                is ApiStateEvent.StateDone -> {
                    progressBar.visibility = View.GONE
                    recyclerView.visibility = View.VISIBLE
                }
            }
        })

        viewModel.searchPhoto(intent.getStringExtra(EXTRA_SEARCH_TEXT))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    companion object {

        const val EXTRA_SEARCH_TEXT = "INTENT_EXTRA_SEARCH_TEXT"

        @JvmStatic
        fun launch(searchText: String, callingActivity: AppCompatActivity) {
            callingActivity.startActivity(Intent(callingActivity, PhotosActivity::class.java).apply {
                putExtra(EXTRA_SEARCH_TEXT, searchText)
            })
        }
    }
}