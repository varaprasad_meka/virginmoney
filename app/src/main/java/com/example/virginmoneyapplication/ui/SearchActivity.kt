package com.example.virginmoneyapplication.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.virginmoneyapplication.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout

class SearchActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        setTitle("Search Photos")

        val searchButton = findViewById<Button>(R.id.search_button)
        val searchTextField = findViewById<TextInputLayout>(R.id.search_text)
        searchButton.setOnClickListener {
            val text = searchTextField.editText!!.text
            if (text.isNullOrEmpty()) {
                showAlertDialog("Search text is empty. Please enter text to search for photos.")
            } else if (text.length > 20) {
                showAlertDialog("Please enter text within 20 characters")
            } else {
                PhotosActivity.launch(text.toString(), this)
            }
        }
    }

    private fun showAlertDialog(message: String) {
        MaterialAlertDialogBuilder(this)
            .setTitle("Sorry")
            .setMessage(message)
            .setPositiveButton("OK") { dialog, which ->
                // Do nothing
            }
            .show()
    }

}