package com.example.virginmoneyapplication.repository

import com.example.virginmoneyapplication.api.FlickrApi
import com.example.virginmoneyapplication.api.FlickrApiResponse
import io.reactivex.Single

class FakeFlickrApi : FlickrApi {

    private val searchResults = mutableListOf<FlickrApiResponse>()

    fun addResult(result: FlickrApiResponse) {
        searchResults.add(result)
    }

    fun clear() {
        searchResults.clear()
    }

    override fun fetchSearchPhotoResults(
        method: String,
        key: String,
        searchTag: String,
        perPage: Int,
        pageNo: Int,
        extras: String,
        format: String,
        noJsonCallback: String
    ): Single<FlickrApiResponse> {
        return Single.just(searchResults[0])
    }
}