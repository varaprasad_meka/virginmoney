package com.example.virginmoneyapplication.repository

import com.example.virginmoneyapplication.api.FlickrApiResponse
import com.example.virginmoneyapplication.api.Photo
import com.example.virginmoneyapplication.api.PhotosDetails
import java.util.concurrent.atomic.AtomicInteger

class FlickrApiResponseFactory {
    private val counter = AtomicInteger(0)
    private val photos = mutableListOf<Photo>()

    fun createFlickrApiResponse() : FlickrApiResponse {
        val photosDetails = PhotosDetails(
            page = 1,
            totalPages = 10,
            photosPerPage = 30,
            totalPhotos = "100",
            photoList = photos
        )
        val response = FlickrApiResponse(
            photosDetails = photosDetails
        )
        return response
    }

    fun addPhoto(photo: Photo) {
        photos.add(photo)
    }

    fun clearPhotos() {
        photos.clear()
    }

    fun createPhoto(): Photo {
        val id = counter.incrementAndGet()
        val photo = Photo(
            id = "id$counter",
            owner = "owner$id",
            secret = "secret$id",
            server = "server$id",
            farm = id,
            title = "title$id",
            isPublic = 0,
            imageUrl = "url$id"
        )
        return photo
    }
}