package com.example.virginmoneyapplication.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.example.virginmoneyapplication.api.Photo
import io.reactivex.disposables.CompositeDisposable
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test

class FlickrSearchPhotosRepositoryTest {

    @get:Rule
    val instantExecutor = InstantTaskExecutorRule()

    private val fakeApi = FakeFlickrApi()
    private val repository = FlickrSearchPhotosRepository(flickrApi = fakeApi)
    private val apiResponseFactory = FlickrApiResponseFactory()

    @Test
    fun emptyList() {
        apiResponseFactory.clearPhotos()
        val searchResults = apiResponseFactory.createFlickrApiResponse()
        fakeApi.addResult(searchResults)
        val listing = repository.searchPhotos(searchText = "Cat", compositeDisposable = CompositeDisposable())
        val pagedList = getPagedList(listing)
        assertThat(pagedList.size, `is`(0))
    }

    @Test
    fun onePhoto() {
        apiResponseFactory.clearPhotos()
        val photo = apiResponseFactory.createPhoto()
        apiResponseFactory.addPhoto(photo)
        val searchResults = apiResponseFactory.createFlickrApiResponse()
        fakeApi.addResult(searchResults)
        val listing = repository.searchPhotos(searchText = "Cat", compositeDisposable = CompositeDisposable())
        assertThat(getPagedList(listing), `is`(listOf(photo)))
    }

    @Test
    fun multiplePhotos() {
        apiResponseFactory.clearPhotos()
        for (i in 1..10) {
            apiResponseFactory.run {
                val photo = createPhoto()
                addPhoto(photo)
            }
        }
        val searchResults = apiResponseFactory.createFlickrApiResponse()
        fakeApi.addResult(searchResults)
        val listing = repository.searchPhotos(searchText = "Cat", compositeDisposable = CompositeDisposable())
        assertThat(getPagedList(listing).size, `is`(10))
    }

    /**
     * extract the latest paged list from the listing
     */
    private fun getPagedList(listing: Listing): PagedList<Photo> {
        val observer = LoggingObserver<PagedList<Photo>>()
        listing.pagedList.observeForever(observer)
        assertThat(observer.value, `is`(CoreMatchers.notNullValue()))
        return observer.value!!
    }

    /**
     * simple observer that logs the latest value it receives
     */
    private class LoggingObserver<T> : Observer<T> {
        var value : T? = null
        override fun onChanged(t: T?) {
            this.value = t
        }
    }
}