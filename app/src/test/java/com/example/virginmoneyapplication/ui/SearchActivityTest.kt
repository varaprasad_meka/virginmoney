package com.example.virginmoneyapplication.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.core.internal.deps.guava.collect.Iterables
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.activityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.ext.truth.content.IntentSubject.assertThat

import com.example.virginmoneyapplication.R
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * This test starts SearchActivity, enters search text in edit text field and selects search button
 * then verifies that Intent to launch PhotosActivity is fired.
 */
@RunWith(AndroidJUnit4::class)
class SearchActivityTest {

    @get:Rule
    var activityScenarioRule = activityScenarioRule<SearchActivity>()

    @Before
    fun intentsInit() {
        Intents.init()
    }

    @After
    fun intentsTeardown() {
        Intents.release()
    }

    @Test
    fun enterSearchText_startsPhotosActivity() {
        onView(withId(R.id.search_edit_text)).perform(typeText("Cat"), closeSoftKeyboard())
        onView(withId(R.id.search_button)).perform(click())
        assertThat(Iterables.getOnlyElement(Intents.getIntents())).hasComponentClass(PhotosActivity::class.java)
    }
}